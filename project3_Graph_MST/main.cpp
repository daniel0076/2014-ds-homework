//Environment:
//gcc4.9 with std=c++11 on Linux
#include "graph.cpp"
#include <fstream>
using namespace std;

int main(int argc,char** argv)
{
    if(argc!=2){
        cout<<"usage: "<<argv[0]<<" [input data]"<<endl;return 1;
    }
    fstream file;
    string input;
    file.open(argv[1]);
    while(getline(file,input)){
            int n=0;
            sscanf(input.c_str(),"%d",&n);
            graph _graph(n);  //new a graph object
            getline(file,input);
            int edge=0;
            sscanf(input.c_str(),"%d",&edge);   //get the edges
            for(int i=0;i<edge;i++){            //insert the edges
                int n1=0,n2=0,w=0;
                getline(file,input);
                sscanf(input.c_str(),"%d%d%d",&n1,&n2,&w);
                _graph.insert(n1,n2,w);
            }
            _graph.mst();                       //find the minimal spanning tree
            getline(file,input);
            int origin=0,end=0;
            while(sscanf(input.c_str(),"%d%d",&origin,&end)==2){ //read the line of shortest path
                _graph.shortest_path(origin,end); //print the shortest path
                getline(file,input);
            }
            if(input[0]=='z'){  //clean the old data
                cout<<endl;
                origin=0;
                end=0;
            }else if(input[0]=='x'){
                cout<<endl;
            }else {
                cout<<"error "<<endl;
            }
    }

    return 0;
}
