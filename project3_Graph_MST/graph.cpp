//use gcc4.9 with c++11 on Linux
#include <iostream>
#include <cstdlib>
#include <climits>
using namespace std;

class node //the node (vertice of the graph)
{
    public:
        node(int v=0,int w=0):value(v),weight(w),next(nullptr){};
        int value;
        int weight;
        node* next;
};
class graph //class graph
{
    public:
        graph(int n=0):total_node(n),nodelist(nullptr){
            nodelist=new node*[n]; //the list of the nodes
            for(int i=0;i<n;i++){
                nodelist[i]=nullptr;
            }
        };
        ~graph(){
            delete[] nodelist;
        };
        bool insert(int,int,int);
        void print(); //print the minimal spanning tree
        void print(int*,int,int); //printer for shortest, only interested in path from origin to end
        void mst(); //minimal spanning tree
        void shortest_path(int,int);
    private:
        node** nodelist;
        int total_node;
};
void graph::print(){
    bool visited[total_node]; //if is visited
    for (int i = 0; i < total_node; i++)visited[i]=false;
    for(int i=0;i<total_node;i++){
        auto it=nodelist[i];
        while(it!=nullptr){
            if(!visited[it->value])
                cout<<"{"<<i<<","<<it->value<<"};";
            it=it->next;
        }
        visited[i]=true;
    }
        cout<<endl;
}
void graph::print(int* parent,int origin,int end){
    int node=origin;
    while(node!=end){
        cout<<"{"<<node<<","<<parent[node]<<"};";
        node=parent[node];
    }
    cout<<endl;
}
void graph::shortest_path(int origin,int end){ //dijisktra algorithm
    graph shortest(total_node);
    bool selected[total_node];
    int distance[total_node];
    int parent[total_node]; //parent of the vertice
    int next[total_node];//which one is the next
    for (int i = 0; i < total_node; i++) { //init
        selected[i]=false;
        distance[i]=INT_MAX;
        parent[i]=0;
        next[i]=0;
    }
    distance[origin]=0;
    selected[origin]=true;
    if(origin==end){
        cout<<"{"<<origin<<","<<end<<"};"<<endl;
        return;
    }
    int target=origin;
    for(int i=0;i<total_node;i++){
        auto it=nodelist[target];
        while(it!=nullptr){ //update vertice info

            if(distance[it->value]==INT_MAX)distance[it->value]=it->weight;
            else distance[it->value]+=it->weight;
            parent[it->value]=target;
            it=it->next;
        }
        int min=INT_MAX;
        int minone=0;
        for(int j=0;j<total_node;j++){ //find the min one
            if(distance[j]<min && !selected[j]){
                min=distance[j];
                minone=j;
                next[parent[j]]=j;
            }
        }
        selected[minone]=true;
        shortest.insert(parent[minone],minone,distance[minone]); //insert to node tree
        target=minone;
        if(minone==end){
            shortest.print(next,origin,end);
            return;
        }
    }
    cout<<"Not exist"<<endl;
}
void graph::mst(){ // prim's algorithm
    graph mst(total_node);
    int distance[total_node];
    int parent[total_node];
    bool visited[total_node];
    for (int i = 0; i < total_node; i++) { //initialize
        distance[i]=INT_MAX;
        visited[i]=false;
        parent[i]=0;
    }
    distance[0]=0;
    visited[0]=true;
    int target=0;
    for(int i=0;i<total_node-1;i++){
        auto it=nodelist[target];
        while(it!=nullptr){ //update the edge weight infomation
            distance[it->value]=it->weight;
            parent[it->value]=target;
            it=it->next;
        }
        int min=INT_MAX;
        int minone=0;
        for(int j=0;j<total_node;j++){ //find the min weight
            if(distance[j]<min && !visited[j]){
                min=distance[j];
                minone=j;
            }
        }
        visited[minone]=true;
        mst.insert(parent[minone],minone,distance[minone]);
        target=minone;
    }
    for(int i=0;i<total_node;i++){
        if (visited[i]==false){
            cout<<"No spanning tree"<<endl;
            return;
        }
    }
    mst.print();
};
bool graph::insert(int n,int v=0,int w=0){
        node* tmp=new node(v,w);
        tmp->next=nodelist[n];
        nodelist[n]=tmp;
        node* tmp_=new node(n,w);
        tmp_->next=nodelist[v];
        nodelist[v]=tmp_;
        return true;
};
