#include <cstdlib>
#include <iostream>
#include <regex>
using namespace std;
class Complex
{
    public:
        Complex(int r=0,int v=0):real_part(r),virtual_part(v){};
        Complex operator+(Complex adden)
        {
            return Complex(this->real_part+adden.real_part,this->virtual_part+adden.virtual_part);
        };
        Complex operator*(Complex multiplier)
        {
            int tmp_virtual=this->virtual_part*multiplier.real_part + this->real_part*multiplier.virtual_part;
            int tmp_real=this->real_part*multiplier.real_part - this->virtual_part*multiplier.virtual_part;
            return Complex(tmp_real,tmp_virtual);
        }
        friend ostream& operator<<(ostream& os,const Complex& com)
        {
            if (com.real_part>0)
            {
                os<<com.real_part;
            }
            if (com.virtual_part != 0)
            {
                if(com.virtual_part>0) os<<"+"<<com.virtual_part<<"i";
                else os<<com.virtual_part<<"i";
            }
            return os;
        }
        friend Complex operator>>(istream& is,Complex& com)
        {
            smatch realm,virtm;
            string tmp;
            is>>tmp;
            regex real("[+-]?[0-9]+[^i]");
            regex virt("[+-][0-9]*i");
            regex_search(tmp,realm,real);
            ssub_match sub=realm[0];
            string base=sub.str();
            com.real_part=atoi(base.c_str());
            regex_search(tmp,virtm,virt);
            sub=virtm[0];
            base=sub.str();
            if(base=="+i") com.virtual_part=1;
            else if(base=="-i") com.virtual_part=-1;
            else com.virtual_part=atoi(base.c_str());
            return com;
        }
    private:
        int real_part;
        int virtual_part;
};
class Rectangle
{
    public:
        Rectangle(int x=0,int y=0,int w=0,int h=0):x(x),y(y),height(h),width(w){};
        int area(void)
        {
            return (width*height);
        }
        int primeter(void)
        {
            return 2*(width+height);
        }
        void intersect(Rectangle rec)
        {
                Rectangle tmp;
                tmp.x=max(this->x,rec.x);
                tmp.y=max(this->y,rec.y);
                tmp.width=min(this->x+this->width,rec.x+rec.width)-tmp.x;
                tmp.height=min(this->y+this->height,rec.y+rec.height)-tmp.y;
                if (tmp.width<=0 || tmp.height <=0 || tmp.height==min(this->height,rec.height) || tmp.width==min(this->height,rec.height)) {
                    cout <<"No intersection"<<endl;
                }
                else
                {
                    cout<<"The intersection rectangle starts at x= "<<tmp.x<<" y= "<<tmp.y<<" with width= "<<tmp.width<<" and height = "<<tmp.height<<" the area is "<<tmp.area()<<", primeter is "<<tmp.primeter()<<endl;
                }
        }
        void bound(Rectangle rec)
        {
            Rectangle tmp;
            tmp.x=min(this->x,rec.x);
            tmp.y=min(this->y,rec.y);
            tmp.width=max(this->x+this->width,rec.x+rec.width)-tmp.x;
            tmp.height=max(this->y+this->height,rec.y+rec.height)-tmp.y;
            cout<<"The minmal bounding rectangle starts at x= "<<tmp.x<<" y= "<<tmp.y<<" with width= "<<tmp.width<<" and height = "<<tmp.height<<" the area is "<<tmp.area()<<", primeter is "<<tmp.primeter()<<endl;
        }
    private:
        int x;
        int y;
        int height;
        int width;
};
int main()
{
    Rectangle rectA(2, 2, 4, 6);
    Rectangle rectB(6, 1, 2, 4);
    Rectangle rectC(4, 4, 1, 1);
    Rectangle rectD(-1, -3, 8, 3);
    cout<<"Rectangle A area= ";
    cout<<rectA.area()<<" primeter "<<rectA.primeter()<<endl;
    cout<<"Rectangle B area= ";
    cout<<rectB.area()<<" primeter "<<rectB.primeter()<<endl;
    cout<<"Rectangle C area= ";
    cout<<rectC.area()<<" primeter "<<rectC.primeter()<<endl;
    cout<<"Rectangle D area= ";
    cout<<rectD.area()<<" primeter "<<rectD.primeter()<<endl;

    cout<<"Rectangle A and Rectangle B: "<<endl;
    rectA.intersect(rectB);
    rectA.bound(rectB);
    cout<<endl;
    cout<<"Rectangle A and Rectangle D: "<<endl;
    rectA.intersect(rectD);
    rectA.bound(rectD);
    cout<<endl;
    cout<<"Rectangle B and Rectangle C: "<<endl;
    rectB.intersect(rectC);
    rectB.bound(rectC);
    cout<<endl;
    cout<<"Rectangle C and Rectangle D: "<<endl;
    rectC.intersect(rectD);
    rectC.bound(rectD);
    cout<<endl;
    Complex a(4,3);
    Complex b(5,-4);
    Complex c,d;
    cout<<"Contructed a and b are "<<a<<" and "<<b<<endl;
    cout<<"a+b is "<<a+b;
    cout<<" and a*b is "<<a*b<<endl;
    cout<<"Please input a comple number: ";
    cin>>c;
    cout<<"Please input another comple number: ";
    cin>>d;
    cout<<"input c and d are "<<c<<" and "<<d<<endl;
    cout<<"a+b+c+d is "<<a+b+c+d;
    cout<<" and a*b*c*d is "<<a*b*c*d<<endl;
    return 0;
}
