//==========================================================//
//Binary Search Tree                                        //
//                                                          //
//author:Daniel Tsai 0216007                                //
//https://github.com/daniel0076                             //
//                                                          //
//This is a program of Binary Search Tree                   //
//inplemented by a linked list,the functions like           //
//insert,delete,get,getbyrank,print in several orders       //
//are provided.                                             //
//                                                          //
//Please use c++11 as the compile standard                  //
//or it will definitely explode!!and so as my score         //
//                                           ______  _       _     _     _
//Tested under Linux/FreeBSD, with gcc-4.8, //     _| |_   _| |_  /|    /|
//                                         ||     |_   _| |_   _| ||    ||
//                            please use   ||       |_|     |_|   ||    ||
//                                          \\_____              _||_  _||_
//===========================================================================
//
// _______     _________
//|  ____ \\   / _______))
//| ||   \ \\ |  ((_____
//| ||   | ||  \ _____  \\     \\this is an art
//| ||___/ //  ______ ) ||
//|_______//  (_________//
//
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cstring>
using namespace std;

//================//
//prototypes here //
//================//

//a shell interact with input and the BSTree object;
void shell(string);

//class TreeNode prototypes
class TreeNode {
    friend class BSTree;
    public:
        TreeNode():leftChild(nullptr),rightChild(nullptr),value(0),key(0),leftSize(1){};
        TreeNode(int k,string val):leftChild(nullptr),\
        rightChild(nullptr),value(val),key(k),leftSize(1){};
    private:
        int key;
        int leftSize;
        string value;
        TreeNode* leftChild;
        TreeNode* rightChild;
};

//class for binary search tree prototypes
class BSTree{
    public:
        BSTree():root(nullptr),count(0){};
        ~BSTree(){
            //a custom destructor
            while(root!=nullptr)root=deleteByKey(root);
                cout<<"Total "<<count<<" nodes cleared"<<endl;
        };
        void insert(int,string);
        void deleteByKey(int);
        void get(int);
        void getByRank(int);
        void printAll();
        void printPreOrder();
        void printPostOrder();
    private:
        //these private function are for recursive call,the public one is API, and it will call the private one
        TreeNode* deleteByKey(TreeNode*);
        const TreeNode* get(int,const TreeNode*);
        void inorder(TreeNode*);
        void printPreOrder(const TreeNode*);
        void printPostOrder(const TreeNode*);
        TreeNode* root;
        int count;
};

//================================================//
//main function,read the inputs and call the shell//
//================================================//

int main(int argc,char** argv)
{
    //waring if the argument is not correct
    if(argc!=2){ cout<<"usage: "<<argv[0]<<" [input data]"<<endl;}
    else {
        shell(argv[1]);
    }
    return 0;
}

//=====================//
//implementations      //
//=====================//

//Class BSTree implementations
void BSTree::deleteByKey(int k){
    TreeNode* it=root;
    TreeNode* parent=it;
    if(k==root->key){ //if the key is root
        root=deleteByKey(root);
        cout<<"Key "<<k<<" deleted"<<endl;
        count--;
        return;
    }
    while(it!=nullptr){
        if(it->key==k)parent=deleteByKey(it); //key is equal to parent,delete it
        else if(k>it->key){ //key is greater than parent,go to rightChild
            it=it->rightChild;
            if(it!=nullptr&&k==it->key){
                parent->rightChild=deleteByKey(it);
                cout<<"Key "<<k<<" deleted"<<endl;
                count--;
                return;
            }
        }else { //key is smaller than parent,go to leftchild
            it=it->leftChild;
            if(it!=nullptr&&k==it->key){
                parent->leftSize--;
                parent->leftChild=deleteByKey(it);
                cout<<"Key "<<k<<" deleted"<<endl;
                count--;
                return;
            }
        }
        parent=it;
    }
    cout<<"error:can't delete, key not found"<<endl;
    return;
}
//recursively delete the key
TreeNode* BSTree::deleteByKey(TreeNode* node){
    if(node->leftChild==nullptr && node->rightChild==nullptr){ //the node has no children
        delete node;
        return nullptr;
    }else if(node->leftChild!=nullptr && node->rightChild!=nullptr){ //the node has two children
        TreeNode* it=node->rightChild; //get the smallest in the rightChild,to replace the root
        if(it->leftChild==nullptr){
            it->leftChild=node->leftChild; //if no leftchild,just get the one iterator pointed to
            delete node;
            return it;
        }else{ //there is a smallest leftchild;get it
            TreeNode* parent;
            while(it->leftChild != nullptr){
                parent=it;
                it=it->leftChild;
            }
            parent->leftSize--;
            node->key=it->key; //replace the node being delete with the smallest in the rightChild tree;
            node->value=it->value;
            delete it;
            parent->leftChild=nullptr;
            return node;
        }
    }else{ //the node has one child,call recursive delete
            TreeNode* tmp=node->leftChild;
        if(node->leftChild!=nullptr){
            tmp=node->leftChild;
        }else{
            tmp=node->rightChild;
        }
            delete node;
            return tmp;
    }
}
//use the "leftsize" the get the rank
void BSTree::getByRank(int r){
    TreeNode* it=root;
    while(it!=nullptr){
        if(r==it->leftSize){cout<<"("<<it->key<<", "<<it->value<<")"<<endl;return; }
        if(r<it->leftSize)it=it->leftChild;
        else{
            r-=it->leftSize;
            it=it->rightChild;
        }
    }
    cout<<"rank not found"<<endl;

//the outer get,print result and call inner get
} void BSTree::get(int k){
    const TreeNode* result=get(k,root);
    if(result==nullptr)cout<<"key "<<k<<" not found"<<endl;
    else cout<<"("<<result->key<<", "<<result->value<<")"<<endl;
}
//the inner get,find the key recursively
const TreeNode* BSTree::get(int k,const TreeNode* node){
    if(node==nullptr)return nullptr;
    if(node->key==k)return node;
    else if(k>node->key)return get(k,node->rightChild);
    else return get(k,node->leftChild);
}
//printers
void BSTree::printPostOrder(){
    if(root==nullptr)cout<<"empty"<<endl;
    else printPostOrder(root);
    cout<<endl;
}
void BSTree::printPostOrder(const TreeNode* node){
    if(node!=nullptr){
        printPostOrder(node->leftChild);
        printPostOrder(node->rightChild);
        cout<<"("<<node->key<<", "<<node->value<<"); ";
    }
}

void BSTree::printPreOrder(){
    if(root==nullptr)cout<<"empty"<<endl;
    else printPreOrder(root);
    cout<<endl;
}
void BSTree::printPreOrder(const TreeNode* node){
    if(node!=nullptr){
        cout<<"("<<node->key<<", "<<node->value<<"); ";
        printPreOrder(node->leftChild);
        printPreOrder(node->rightChild);
    }
}

void BSTree::printAll(){
    if(root==nullptr)cout<<"empty"<<endl;
    else inorder(root);
    cout<<endl;
}
void BSTree::inorder(TreeNode* node){
    if(node!=nullptr){
        inorder(node->leftChild);
        cout<<"("<<node->key<<", "<<node->value<<"); ";
        inorder(node->rightChild);
    }
}

void BSTree::insert(int key,string value){
    TreeNode* parent;
    TreeNode* it=root;
    cout<<key<<" inserted"<<endl;
    count++;
    if(root==nullptr){
        root=new TreeNode(key,value);
    }
    else{
        while(it){
            parent=it;
            if(key>it->key){
                it=it->rightChild;
            }
            else {
                it=it->leftChild;
                parent->leftSize++;
            }
        }
        it=new TreeNode(key,value);
        if(key>parent->key){
            parent->rightChild=it;
        }
        else {
            parent->leftChild=it;
        }
    }
}

//the shell read the input
void shell(string fname){
    BSTree bst;
    fstream file;
    string tmp;
    file.open(fname.c_str());
    if(file){
        while(getline(file,tmp)){
            char ins[1]="",value[200]="";
            int key=0;
            sscanf(tmp.c_str(),"%c %d %[^\r\n\t]",ins,&key,value);
            switch(*ins){
                case 'I':
                    bst.insert(key,string(value));
                    break;
                case 'G':
                    bst.get(key);
                    break;
                case 'R':
                    bst.getByRank(key);
                    break;
                case 'P':
                    bst.printAll();
                    break;
                case 'O':
                    bst.printPreOrder();
                    break;
                case 'Q':
                    bst.printPostOrder();
                    break;
                case 'D':
                    bst.deleteByKey(key);
                    break;
                case 'E':
                    return;
                    break;
            }
        }
    }
}
