#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;

class polynomial
{
    public:
        polynomial():index(-1),now(0){};
        void read (string input)
        {
            this->index=-1;
            this->now=0;
            char* tr=strtok((char*)input.c_str(),"+");
            while(tr!= NULL)
            {
                int co,fac,count=0;
                char useless;
                count=sscanf(tr,"%d%s%d",&co,&useless,&fac);
                this->index++;
                this->data[0][this->index]=co;
                if(count == 2)      this->data[1][this->index]=1;
                else if(count==1)   this->data[1][this->index]=0;
                else if(count==0){
                    this->data[0][this->index]=1;
                    this->data[1][this->index]=1;
                }
                else                this->data[1][this->index]=fac;
                tr=strtok(NULL,"+");
                co=-1;
            }
        }
        polynomial operator+ (polynomial poly)
        {
            polynomial tmp;
            while(this->now<=this->index || poly.now<=poly.index)
            {
                if (this->get_factor(this->now)==poly.get_factor(poly.now)) {
                    tmp.insert(this->get_coef(this->now)+poly.get_coef(poly.now) , this->get_factor(this->now));
                    this->now++;
                    poly.now++;
                }else
                {
                    polynomial* ptr;
                    if(this->now > this->index)     ptr=&poly;
                    else if(poly.now > poly.index)  ptr=this;
                    else ptr=(this->get_factor(this->now) > poly.get_factor(poly.now)) ? this:&poly;
                    tmp.insert(ptr->get_coef(ptr->now),ptr->get_factor(ptr->now));
                    ptr->now++;
                }
            }
            this->now=0;
            poly.now=0;
            tmp.sort();
            return tmp;
        }
        polynomial operator* (polynomial poly)
        {
            int x_co,x_fac;
            polynomial tmp;
            for(int t=0;t<=this->index;t++) {
                for(int p=0;p<=poly.index;p++) {
                    x_co=this->get_coef(t)*poly.get_coef(p);
                    x_fac=this->get_factor(t)+poly.get_factor(p);
                    tmp.insert(x_co,x_fac,tmp.find_pos(x_fac));
                }
            }
            tmp.sort();
            return tmp;
        }
        void sort(void)
        {
            int max;
            for(int i=0;i<this->index;i++) {
                max=i;
                for(int j=i+1;j<=this->index;j++){
                    if(this->get_factor(j)>this->get_factor(max)){
                        max=j;
                    }
                }
                if(max!=i){
                    swap(this->data[0][i],this->data[0][max]);
                    swap(this->data[1][i],this->data[1][max]);
                }
            }
        }
        void insert(int co,int fac)
        {
            this->index++;
            this->data[0][this->index]=co;
            this->data[1][this->index]=fac;
        }
        void insert(int co,int fac,int pos)
        {
            if(this->data[1][pos]==fac){
                this->data[0][pos]+=co;
            }else{
                this->data[0][pos]=co;
                this->data[1][pos]=fac;
            }
        }
        int get_coef(int i)
        {
            return this->data[0][i];
        }
        int get_factor(int i)
        {
            return this->data[1][i];
        }
        void print(void)
        {
            int c=0;
            while(c <= this->index) {
                switch (this->data[1][c]) {
                    case 0:
                        cout<<this->data[0][c];
                        break;
                    case 1:
                        cout<<this->data[0][c]<<" x ";
                        if(c!=index)cout<<"+ ";
                        break;
                    default:
                        cout<<this->data[0][c]<<" x "<<this->data[1][c];
                        if(c!=index)cout<<" + ";
                        break;
                }
                c++;
            }
            cout<<endl;
        }
    private:
        int find_pos(int fac)
        {
            int c=0;
            while(c<=this->index)
            {
                if(this->data[1][c]==fac) return c;
                c++;
            }
            data[0][index+1]=0; //clean the value left in the memory
            return ++this->index;
        }
        int data[2][20];
        int index;
        int now;
};
void ui(void)
{
    polynomial poly,poly2,tmp;
    string input;
    while(1)
    {
        getline(cin,input);
        if(input=="+"){
            getline(cin,input);
            poly2.read(input);
            tmp=poly+poly2;
            poly=tmp;
        }else if(input=="*"){
            getline(cin,input);
            poly2.read(input);
            tmp=poly*poly2;
            poly=tmp;
        }else if(input=="="){
            poly.print();
            break;
        }
        else {
            poly.read(input);
        }
    }
}

int main()
{
    ui();
    return 0;
}
