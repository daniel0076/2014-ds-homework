#include <iostream>
#include <iomanip>
#include<cstdio>
#include <cstdlib>
#include <fstream>
#define MAX 150 //macro as the MAX of the player_list
using namespace std;

class player{ //a player's record
    public:
        player(string n,double ab_,double h_,double hr_,double tb_,double bb_):
            name(n),data{ab_,h_,hr_,tb_,bb_,h_/ab_,(h_+bb_)/(ab_+bb_),tb_/ab_,(h_+bb_)/(ab_+bb_)+(tb_/ab_)}
        {};
        double get_value(int index){return data[index];};
        string get_name(){return name;};
        void print(int index1,int index2){
            cout<<"("<<name<<", "<<setprecision(3)<<data[index1]<<", "<<data[index2]<<");";
        }
    private:
        string name;
        double data[9];
};
class player_list{ // a list of player's record,implemented by array
    public:
        player_list():list{nullptr},top(-1){};
        ~player_list(){
            while(top!=-1){
                delete list[top];
                top--;
            }
        }
        bool insert(string,double,double,double,double,double);
        player* operator[](int);
        void quicksort(int ,const int,const int);
        void heap_adjust(player**,const int,const int,const int);
        void heapsort(int ,int,int);
        void sort(int,int); //metafunction to call quicksort and heapsort
        void print(int,int);
    private:
        int top; //top of the player_list
        player* list[MAX]; //the player array
};

void player_list::print(int index1,int index2){
    for(int i=0;i<=top;i++){
        list[i]->print(index1,index2);
    }
    cout<<endl;
};
//metafunction to call quicksort and heapsort
void player_list::sort(int index1,int index2){
    quicksort(index1,0,top);
    int begin=0;
    for(int i=1;i<=top;i++){
        if(list[i]->get_value(index1)!=list[i-1]->get_value(index1)){
            heapsort(index2,begin,i-1);
            begin=i;
        }
    }
    heapsort(index2,begin,top);
    print(index1,index2);
}
//heapsort
void player_list::heapsort(int index,int begin,int end){
    int n=end-begin+1;
    for(int i=n/2-1;i>=0;i--){
        heap_adjust(list+begin,n,i,index);
    }

    auto list_=list+begin;
    for(int i=n-1;i>0;i--){
        auto tmp=list_[0];list_[0]=list_[i];list_[i]=tmp;
        heap_adjust(list+begin,i,0,index);
    }
}
//heap adjust
void player_list::heap_adjust(player** list_,const int n,const int key,int index){
    int lchild=[](int k){return 2*k+1;}(key);
    int rchild=[](int k){return 2*k+2;}(key);
    if(lchild<n){
        if(list_[key]->get_value(index)>list_[lchild]->get_value(index)){
            auto tmp=list_[key];
            list_[key]=list_[lchild];
            list_[lchild]=tmp;
            heap_adjust(list_,n,lchild,index);
        }
     }
    if(rchild<n){
        if(list_[key]->get_value(index)>list_[rchild]->get_value(index)){
            auto tmp=list_[key];
            list_[key]=list_[rchild];
            list_[rchild]=tmp;
            heap_adjust(list_,n,rchild,index);
        }
    }
}
//quicksort
void player_list::quicksort(int key,const int left,const int right){
    if(left<right){
        int l=left+1,r=right;
        double pivot=list[left]->get_value(key);
        while(l<=r){
            while(list[l]->get_value(key)>=pivot){
                l++;
                if(l>=right)break;
            }
            while(list[r]->get_value(key)<=pivot){
                r--;
                if(r<=left)break;
            }
            if(l<r){
                auto tmp=list[l];
                list[l]=list[r];
                list[r]=tmp;
            }else break;

        }
            auto tmp=list[left];
            list[left]=list[r];
            list[r]=tmp;
            quicksort(key,left,r-1);
            quicksort(key,r+1,right);
    }
}
//provide the array like operator rewrite
player* player_list::operator[](int index){
    if(index<=top){
        return list[index];
    }
    else{
        return nullptr;
    }
}
//insert into the array
bool player_list::insert(string n,double ab_,double h_,double hr_,double tb_,double bb_){
    player* tmp=new player(n,ab_,h_,hr_,tb_,bb_);
    if(top<MAX){
        list[++top]=tmp;
    }else{
        cout<<"array full"<<endl;
        return false;
    }
};

int main(int argc, char const** argv)
{
    if(argc!=2){
        cout<<"usage: "<<argv[0]<<" [input data]"<<endl;return 1;
    }
    fstream file;
    player_list p;
    file.open(argv[1]);
    string input;
    string convert[9]={"AB","H","HR","TB","BB","AVG","OBP","SLG","OPS"}; //convert input to indexes
    char i1[5],i2[5]; //index1,index2 in english
    int index1,index2;
    char name[100];
    int data[5];//five integert data
    while(getline(file,input)){
        switch(input[0]){
            case 'I':
                sscanf(input.c_str(),"%*s%s%d%d%d%d%d",&name,&data[0],&data[1],&data[2],&data[3],&data[4]);
                p.insert(name,data[0],data[1],data[2],data[3],data[4]);
                break;
            case 'S':
                sscanf(input.c_str(),"%*s%s%s",&i1,&i2);
                for(int i=0;i<9;i++){
                    if(i1==convert[i])index1=i;
                    if(i2==convert[i])index2=i;
                }
                p.sort(index1,index2);
                break;
            case 'C':
                p.~player_list();
                break;
            case 'E':
                return 0;
        }
    }
    return 0;
}
