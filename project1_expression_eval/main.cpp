//==========================================================//
//Expression Evaluator                                      //
//                                                          //
//author:Daniel Tsai 0216007                                //
//https://github.com/daniel0076                             //
//                                                          //
//This is a program that turn infix expression              //
//to postfix expression,and evaluate it                     //
//The program has the ability to detect                     //
//overflow,divid by zero and the validity of the expression //
//                                                          //
//Please use c++11 as the compile standard                  //
//or it will definitely explode!!and so as my score         //
//                                           ______  _       _     _     _
//Tested under Linux/FreeBSD, with gcc-4.8, //     _| |_   _| |_  /|    /|
//                                         ||     |_   _| |_   _| ||    ||
//                            please use   ||       |_|     |_|   ||    ||
//                                          \\_____              _||_  _||_
//===========================================================================
//
// _______     _________
//|  ____ \\   / _______))
//| ||   \ \\ |  ((_____
//| ||   | ||  \ _____  \\     \\this is an art
//| ||___/ //  ______ ) ||
//|_______//  (_________//
//
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <cctype>
#include <sstream>
#include <climits>
#include <cstdlib>
using namespace std;
//hand-made stack implementation,fresh and cheap to use
class Mystack //prototypes here
{
    public:
        Mystack():_top(nullptr){}
        ~Mystack(){while(!isEmpty())pop();} //destroy everything
        void push(string in);
        void pop();
        const string& top()const; //just observe the top...I promise I won't change anything
        string& top(); //maybe I want to change something,so no const here
        bool isEmpty()const;//this comment is not empty
        void print()const; //print the stack
    private:
        struct node;
        node* _top;
};
struct Mystack::node //hand-made node,with string data-type,fresh and easy
{
    node(string in,node* s):datum(in),succ(s){} //node constructor
    string datum;
    node* succ; //this means successive, not successful or other...
};
class evaluate  //class evaluate, the prototype
{
    public:
        void load_from_file(string);    //load the string from file
        void to_postfix(string);        //go to postfix!
        bool priority(string in_come);  //check the precedence
        bool is_overflow(string);       //are and int overflow?
        bool is_overflow(int,int,char); //are int +-*/ int overflow?
        void eval_postfix();            //get the result
        void reset();                   //bye
    private:
        Mystack postfix;                //stack for postfix expression
        Mystack oper;                   //stack for operators
        Mystack temp;                   //stack for operands and for generating the postfix expression
};
//==================================//
//class Mystack implementations here//
//==================================//
inline bool Mystack::isEmpty()const{return _top==nullptr;};
inline const string& Mystack::top()const{return _top->datum;};
inline string& Mystack::top(){return _top->datum;};
void Mystack::print()const
{
    node* it=_top;
    if(!isEmpty()){
        while(it->succ!=nullptr)
        {
            cout<<it->datum<<" ";
            it=it->succ;
        }
        cout<<it->datum<<" "<<endl;
    }else cout<<"stack is empty"<<endl;
};
void Mystack::push(string in) //push a new item to the stack
{
    if(isEmpty()){
        _top=new node(in,nullptr); //new a space in memory for it
    }else{
        node* tmp=_top;
        _top=new node(in,nullptr);
        _top->succ=tmp;
    }
};
void Mystack::pop()
{
    if(!isEmpty()){
        node* tmp=_top;
        _top=_top->succ;
        delete tmp;
    }
}
//===================================//
//Class evaluate Implementations here//
//===================================//
void evaluate::reset()
{
    postfix.~Mystack();
    oper.~Mystack();
    temp.~Mystack();
}
bool evaluate::is_overflow(string in) //test if a int is overflow
{                                     //use stringstream to convert int to string
    stringstream ss;
    int test=atoi(in.c_str());        //let it overflow
    ss<<test;                         //convert it to string
    if(strcmp(in.c_str(),ss.str().c_str())){  //compare if the string and the int are the same
        return true;
    } else return false;
}
//check if the expression will overflow
void evaluate::load_from_file(string fname)
{
    fstream file;
    string tmp;
    file.open(fname.c_str());
    if(file){
        while(getline(file,tmp))
        {
            to_postfix(tmp);   //get each line and send it to the converter
        }
    }else cout<<"File open error"<<endl;
    file.close();
}
//the infix to postfix converter
void evaluate::to_postfix(string input)
{
    int i=0,paren_count=0,operator_count=0,operand_count=0; //types of counter
    bool error_flag=0;          //error flag
    bool overflow_flag=0;       //overflow flag
    char infix[100000]={};
    strcpy(infix,input.c_str());
    char* token=strtok(infix," \r");
    while(token!=NULL)
    {
        if(isdigit(token[0]) || (token[0]=='-' && strlen(token)>1)){ //if it is a digit!warning:isdigit will fail when negative
            if(is_overflow(token)){     //check overflow
                overflow_flag=1;
            }
            temp.push(token);
            operand_count++;
            if(operand_count!=operator_count+1){
                error_flag=1;
                break;
            }
        }
        else if(*token=='(') {          //operations with ( and  )
            oper.push(token);
            paren_count++;
        }
        else if(*token==')'){
            if(!oper.isEmpty() && operator_count!=0) temp.push(oper.top());//put the operator to the temp stack
            oper.pop();
            oper.pop();
            paren_count--;
        }else{
            if(priority(token)){
                temp.push(oper.top());
                oper.pop();
                oper.push(token);
            }else{
                oper.push(token);
            }
            operator_count++;
            if(operator_count!=operand_count){
                error_flag=1;
                break;
            }
        }
        i++;
        token=strtok(NULL," \r");
    }
    if(paren_count!=0 || operand_count!=operator_count+1){
        cout<<"Invalid expression"<<endl;
        error_flag=1;
    }
    if(!error_flag){
        while(!oper.isEmpty()){       //revert the stack so it become postfix
            temp.push(oper.top());
            oper.pop();
        }
        while(!temp.isEmpty()){
            postfix.push(temp.top());
            temp.pop();
        }
        postfix.print();
        if(!overflow_flag)eval_postfix();
    }else {
        cout<<"Invalid expression"<<endl;
    }
    if(overflow_flag){
        cout<<"Overflow"<<endl;
    }
    reset();
}
bool evaluate::is_overflow(int num1,int num2,char oper)
{
    int test=0;
    long test_long=0;
    switch(oper){
        case '+':
            test_long=long(num1)+long(num2);
            if(test_long>INT_MAX || test_long<INT_MIN){
                cout<<"Overflow"<<endl;
                return true;
            }
            break;
        case '-':
            test_long=long(num1)-long(num2);
            if(test_long>INT_MAX || test_long<INT_MIN){
                cout<<"Overflow"<<endl;
                return true;
            }
            break;
        case '*':
            test=num1*num2;  //if overflow, let it overflow
            if(test/num1!=num2){  //check if if it has overflowed
                cout<<"Overflow"<<endl;
                return true;
            }
            break;
        case '/':
            if(num2==0){     //detect divide by zero
                cout<<"Divide by zero"<<endl;
                reset();
                return true;
                break;
            }
            test=num1/num2;
            if(test==0) return false;
            if(test*num2!=num1){
                cout<<"Overflow"<<endl;
                return true;
            }
            break;
    }
    return false;
}
bool evaluate::priority(string in_come)
{
    if(!oper.isEmpty()){
        string in_Mystack=oper.top();
        if(in_come=="+" || in_come == "-"){  //+- has less priority than */
            if(in_Mystack!="(") return true;
        }
    }
    return false;
}
void evaluate::eval_postfix()
{
    while(!postfix.isEmpty())
    {
        char* top=(char*)postfix.top().c_str(); //get the first item from the postfix stack
        postfix.pop();
        int num1=0,num2=0;
        stringstream ss;
        if(isdigit(top[0]) || (top[0]=='-' && strlen(top)>1)) //if it is a digit
        {
            temp.push(top);                                   //put into stack
        }else {           //if it is a operator
            num2=atoi(temp.top().c_str()); //pop two operand from the stack
            temp.pop();                    //and calculate it
            num1=atoi(temp.top().c_str());
            switch(*top){
                case '+':
                    if(is_overflow(num1,num2,'+')){
                        reset();
                        return ;
                    }
                    ss<<num1+num2;
                    temp.pop();
                    temp.push(ss.str());
                    break;
                case '-':
                    if(is_overflow(num1,num2,'-')){
                        reset();
                        return ;
                    }
                    ss<<num1-num2;
                    temp.pop();
                    temp.push(ss.str());
                    break;
                case '*':
                    if(is_overflow(num1,num2,'*')){
                        reset();
                        return ;
                    }
                    ss<<num1*num2;
                    temp.pop();
                    temp.push(ss.str());
                    break;
                case '/':
                    if(is_overflow(num1,num2,'/')){
                        reset();
                        return ;
                    }
                    ss<<num1/num2;
                    temp.pop();
                    temp.push(ss.str());
                    break;
            }
        }
    }
    temp.print();
    reset();
}
int main(int argc,char** argv) //this is a 5 lines main
{
    if(argc!=2){ cout<<"usage: "<<argv[0]<<" [input data]"<<endl;}
    else {
        evaluate ev;
        ev.load_from_file(argv[1]);
    }
}
